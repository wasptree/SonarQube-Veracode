
## release 0.0.1
- Initial release
- Tested with SonarQube 6.7.1-LTS

## release 0.0.2
- Cleanly skip Veracode processing if no appName set in the sonar-project.properties file
- Cleaned up some log messages
